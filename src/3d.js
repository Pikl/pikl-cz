import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'

function scene() {
  var sceneX = 800
  var sceneY = 800
  var scene = new THREE.Scene()
  var camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.5,
    10000
  )

  var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
  renderer.setSize(sceneX, sceneY)
  document.getElementById('three-renderer').appendChild(renderer.domElement)

  const loader = new GLTFLoader()
  loader.load(
    './../assets/models/testblender.glb',
    function (gltf) {
      gltf.scene.position.x = 2
      gltf.scene.position.y = 1

      scene.add(gltf.scene)
      var animate = function () {
        requestAnimationFrame(animate)

        // gltf.scene.rotation.x += 0.05
        gltf.scene.rotation.y += 0.01

        renderer.render(scene, camera)
      }

      animate()
    },
    function (xhr) {
      console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
    },
    function (error) {
      console.error(error)
    }
  )

  var geometry = new THREE.BoxGeometry(1, 1, 1)
  var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 })
  var cube = new THREE.Mesh(geometry, material)
  scene.add(cube)

  const geometryCube = new THREE.BoxGeometry(1, 1, 1)
  const materialCube = new THREE.MeshBasicMaterial({ color: 0x00ff00 })
  const cube2 = new THREE.Mesh(geometryCube, materialCube)
  scene.add(cube2)

  const geometryS = new THREE.SphereGeometry(1, 32, 32)
  const materialS = new THREE.MeshBasicMaterial({ color: 0xcccccc })
  const sphere = new THREE.Mesh(geometryS, materialS)
  sphere.position.x = 2
  scene.add(sphere)

  camera.position.z = 10

  var animate = function () {
    requestAnimationFrame(animate)

    cube.rotation.x += 0.02
    cube.rotation.y += 0.01

    cube2.rotation.x += 0.05
    cube2.rotation.y += 0.05

    renderer.render(scene, camera)
  }

  animate()
}

export { scene }
